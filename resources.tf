# Create VPC
resource "aws_vpc" "aws_fargate_vpc" {
  cidr_block       = var.vpc_cidr_block
  instance_tenancy = var.tenancy
  tags = {
    "Name" = "aws_fargate_vpc"
  }
}

# Create desired number of private and public subnet within a given range
resource "aws_subnet" "aws_fargate_subnet_private" {
  count             = length(var.availability_zones)
  availability_zone = element(var.availability_zones, count.index)
  vpc_id            = aws_vpc.aws_fargate_vpc.id
  cidr_block        = element(var.subnet_private_cidr, count.index)
  tags = {
    "Name" = "aws_fargate_private_${count.index + 1}"
  }

}
resource "aws_subnet" "aws_fargate_subnet_public" {
  count                   = length(var.availability_zones)
  availability_zone       = element(var.availability_zones, count.index)
  vpc_id                  = aws_vpc.aws_fargate_vpc.id
  cidr_block              = element(var.subnet_public_cidr, count.index)
  map_public_ip_on_launch = true

  tags = {
    "Name" = "aws_fargate_public_${count.index + 1}"
  }
}

# Create an internet gateway
resource "aws_internet_gateway" "gw" {
  depends_on = [
    aws_vpc.aws_fargate_vpc
  ]
  vpc_id = aws_vpc.aws_fargate_vpc.id

  tags = {
    "Name" = "aws_fargate_gw"
  }
}

# Create public and private routes to have local and internet access
resource "aws_route_table" "aws_fargate_rt_public" {
  depends_on = [
    aws_internet_gateway.aws_fargate_gw
  ]
  vpc_id = aws_vpc.aws_fargate_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.aws_fargate_gw.id
  }

  tags = {
    "Name" = "aws_fargate_public_routes"
  }
}
resource "aws_route_table" "aws_fargate_rt_private" {
  vpc_id = aws_vpc.aws_fargate_vpc.id
  tags = {
    "Name" = "aws_fargate_private_routes"
  }
}
resource "aws_route_table_association" "public" {
  count          = length(aws_subnet.aws_fargate_subnet_public.*.id)
  subnet_id      = element(aws_subnet.aws_fargate_subnet_public.*.id, count.index)
  route_table_id = aws_route_table.aws_fargate_rt_public.id
}
resource "aws_route_table_association" "private" {
  count          = length(aws_subnet.aws_fargate_subnet_private.*.id)
  subnet_id      = element(aws_subnet.aws_fargate_subnet_private.*.id, count.index)
  route_table_id = aws_route_table.aws_fargate_rt_private.id

}

# Create Elastic IP
resource "aws_eip" "aws_fargate_ecs_eip" {
  vpc = true
}
resource "aws_nat_gateway" "aws_fargate_ecs_nat" {
  depends_on = [
    aws_subnet.aws_fargate_subnet_private
  ]
  allocation_id = aws_eip.aws_fargate_ecs_eip.id
  subnet_id     = aws_subnet.aws_fargate_subnet_public.0.id

}

# Create nat gateway and associated routes
resource "aws_route" "aws_fargate_nat_route" {
  route_table_id         = aws_route_table.aws_fargate_subnet_private.id
  nat_gateway_id         = aws_nat_gateway.aws_fargate_ecs_nat.id
  destination_cidr_block = "0.0.0.0/0"
}

# Create IAM role for tasks
data "aws_iam_policy_document" "aws_fargate_task_role_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}
resource "aws_iam_role" "aws_fargate_task_role" {
  name               = "aws_fargate_task_role"
  assume_role_policy = data.aws_iam_policy_document.aws_fargate_task_role_assume_role_policy.json
}
resource "aws_iam_role_policy" "aws_fargate_policy" {
  name   = "aws_fargate_policy"
  role   = aws_iam_role.aws_fargate_task_role.id
  policy = file("policies/task_execution_policies.json")
}

# Create ECS Role for EC2 instances
data "aws_iam_policy_document" "aws_fargate_instance_role_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}
resource "aws_iam_role" "aws_fargate_instance_role" {
  name               = "aws_fargate_instance_role"
  assume_role_policy = data.aws_iam_policy_document.aws_fargate_instance_role_assume_role_policy.json
}
resource "aws_iam_role_policy" "aws_fargate_instance_policy" {
  name   = "aws_fargate_instance_policy"
  role   = aws_iam_role.aws_fargate_instance_role.id
  policy = file("policies/instance_policies.json")
}
resource "aws_iam_instance_profile" "ecs_role" {
  name = "aws_fargate_ecs_role"
  role = aws_iam_role.aws_fargate_instance_role.name
}
