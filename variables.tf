# Provider Configuration
variable "access_key" {
  description = "Access key to AWS console."
  type = string
}
variable "secret_key" {
  description = "Secret key to AWS console."
  type = string
}
variable "region" {
  description = "Region of AWS VPC."
  type = string
  default = "us-east"
}

# Tenancy
variable "tenancy" {
  description = "want shared or dedicated tenancy."
  type        = string
  default     = "default"
}

# VPC
variable "vpc_cidr_block" {
  type        = string
  description = "The cidr block of the VPC."
  default     = "10.0.0.0/16"
}

variable "subnet_private_cidr" {
  description = "The cidr blocks for private subnets."
  type        = list(string)
  default     = ["10.0.1.0/24"]
}

variable "subnet_public_cidr" {
  description = "The cidr blocks for public subnets."
  type        = list(string)
  default     = ["10.0.2.0/24"]
}

# Availability Zones
variable "availability_zones" {
  description = "Availability zones."
  type        = list(string)
  default     = ["us-east-1a"]
}

