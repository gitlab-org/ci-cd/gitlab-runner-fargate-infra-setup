# Inpiration taken from : 
# https://rathiaman13.medium.com/creating-ec2-based-ecs-cluster-with-terraform-10b490f3e33c

provider "aws" {
  region     = var.region
  access_key = var.access_key
  secret_key = var.secret_key
}
